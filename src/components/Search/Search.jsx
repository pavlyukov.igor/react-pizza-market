import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import "./Search.css";

class Search extends Component {
  render() {
    const { handleSearch } = this.props;
    return (
      <header className="app-search">
        <Row>
          <Col>
            <div className="app-search--outer">
              <input type="text" onChange={handleSearch} />
            </div>
          </Col>
        </Row>
      </header>
    );
  }
}

export default Search;
