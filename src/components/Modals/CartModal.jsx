import React from "react";
import {
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import cartIcon from "../../assets/images/shopping-cart.svg";

import "./Modals.css";

class CartModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  getProductsList = () => {
    const { cart, removeFromCart } = this.props;
    if (cart.length === 0)
      return <div className="text-center w-100">No products</div>;

    return cart.map((product, index) => (
      <Col key={index} className="mb-2">
        <div className="cart-product-item d-flex justify-content-between align-items-center">
          <div className="d-flex align-items-center">
            <div className="cart-product-item--photo">
              <img
                src={product.photo}
                alt={product.title}
                className="img-fluid"
              />
            </div>
            <div className="cart-product-item--title">{product.title}</div>
          </div>
          <div>
            <div className="cart-product-item--price">
              ${product.price.toFixed(2)}
            </div>
            <button className="cart-product-item--remove" href="#" onClick={() => removeFromCart(product.id)}>x</button>
          </div>
        </div>
      </Col>
    ));
  };

  getTotal = () => {
    const { cart } = this.props;

    return cart.reduce((acc, v) => acc + v.price, 0).toFixed(2);
  };

  render() {
    const { cart } = this.props;
    return (
      <div className="d-flex align-items-center justify-content-end">
        <Button
          className="app-header--cart-button"
          onClick={() => this.toggle()}
        >
          <img src={cartIcon} alt="Shopping cart" />
        </Button>
        <span className="app-header--cart-count">
          {cart.length ? cart.length : ""}
        </span>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className="app-header--cart-modal"
        >
          <ModalHeader toggle={this.toggle}>Your Orders</ModalHeader>
          <ModalBody>
            {this.getProductsList()}
            {cart.length ? (
              <Col>
                <div className="d-flex justify-content-between align-items-center">
                  <h3>Total:</h3>
                  <h4>${this.getTotal()}</h4>
                </div>
              </Col>
            ) : null}
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggle}>
              Order
            </Button>{" "}
            <Button color="secondary" onClick={this.toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default CartModal;
