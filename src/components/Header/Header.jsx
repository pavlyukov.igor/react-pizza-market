import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import CartModal from "../Modals/CartModal.jsx";

import "./Header.css";
import logoImg from "../../assets/images/logo.jpg";

class Header extends Component {
  render() {
    const { cart, removeFromCart } = this.props;
    return (
      <header className="app-header">
        <Row className="d-flex align-items-center">
          <Col xs={10}>
            <div className="d-flex align-items-center app-header--title">
              <img src={logoImg} alt="Pizza" />
              <h2>Pizzeria</h2>
            </div>
          </Col>
          <Col xs={2}>
            <div className="app-header--cart text-right">
              <CartModal cart={cart} removeFromCart={removeFromCart}/>
            </div>
          </Col>
        </Row>
      </header>
    );
  }
}

export default Header;
