import React, { Component } from "react";
import { Col } from "reactstrap";
import "./ProductItem.css";

class ProductItem extends Component {
  render() {
    const { title, desc, photo, price, id, addToCart } = this.props;
    return (
      <Col md={6} lg={4} className="mb-5">
        <div className="product-item d-flex flex-column justify-content-between">
          <div className="d-flex flex-column">
            <div className="title-product">{title}</div>
            <div className="product-item--photo">
              <img src={photo} alt={title} className="img-fluid" />
            </div>
            <div className="product-item--desc">{desc}</div>
            <div className="product-item--price">${price.toFixed(2)}</div>
          </div>
          <div>
            <div className="product-item--order">
              <button onClick={() => addToCart(id)}>I want it!</button>
            </div>
          </div>
        </div>
      </Col>
    );
  }
}

export default ProductItem;
