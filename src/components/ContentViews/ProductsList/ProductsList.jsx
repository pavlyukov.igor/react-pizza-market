import React, { Component } from "react";
import ProductItem from "../ProductItem/ProductItem.jsx";
import { Row } from "reactstrap";
import "./ProductsList.css";

class ProductsList extends Component {
  getProductsList = () => {
    const { products, cart, addToCart } = this.props;
    if (products.length === 0)
      return <div className="text-center w-100">No products</div>;

    return products.map((product, index) => (
      <ProductItem
        title={product.title}
        desc={product.desc}
        id={product.id}
        key={index}
        price={product.price}
        photo={product.photo}
        cart={cart}
        addToCart={addToCart} 
      />
    ));
  };

  render() {
    return (
      <div className="products-list">
        <Row>{this.getProductsList()}</Row>
      </div>
    );
  }
}

export default ProductsList;
