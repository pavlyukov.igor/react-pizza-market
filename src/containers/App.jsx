import React, { Fragment, Component } from "react";
import Header from "../components/Header/Header.jsx";
import Search from "../components/Search/Search.jsx";
import ProductsList from "../components/ContentViews/ProductsList/ProductsList.jsx";

import "./App.css";
import { Container } from "reactstrap";

class App extends Component {
  state = {
    products: [
      {
        id: Math.random(),
        title: "Slice of The Big Times Square Pizza",
        desc: `Sauce on top, sliced fresh mozzarella, pecorino Romano & a touch of extra virgin olive oil. Add from our selection of toppings for add'l charge.`,
        price: 3.99,
        photo: `../assets/images/pizza/pizza-1.jpg`
      },
      {
        id: Math.random(),
        title: "Slice of Grand Central Grandma Pizza",
        desc: `Our famous thin crust plum tomato pizza.`,
        price: 3.1,
        photo: `../assets/images/pizza/pizza-2.jpg`
      },
      {
        id: Math.random(),
        title: "Slice of Chelsea Honey Whole Wheat Grandma Pizza",
        desc: `Plum tomato, fresh mozzarella, pesto, basil, on a honey whole wheat grandma style crust. Add from our selection of toppings for add'l charge.`,
        price: 3.2,
        photo: `../assets/images/pizza/pizza-3.jpg`
      },
      {
        id: Math.random(),
        title: "	Slice of Tribeca Sweet Chili Chicken Pizza",
        desc: `Sweet & spicy Asian chicken.`,
        price: 4.5,
        photo: `../assets/images/pizza/pizza-4.jpg`
      },
      {
        id: Math.random(),
        title: "Slice of Flat Iron Crispino Pizza",
        desc: `Fresh mozzarella. roasted red peppers, tomato, basil, topped with a drizzle of balsamic glaze.`,
        price: 3.2,
        photo: `../assets/images/pizza/pizza-5.jpg`
      },
      {
        id: Math.random(),
        title: "Slice of Long Island Margarita Pizza",
        desc: `Signature TBSONY plum tomato sauce, imported Buffalo mozzarella, basil & a touch of extra virgin olive oil. Add from our selection of toppings for add'l charge.`,
        price: 15.0,
        photo: `../assets/images/pizza/pizza-6.jpg`
      }
    ],
    searchResults: [],
    cart: []
  };

  componentWillMount = () => {
    this.setState({ searchResults: this.state.products });
  };

  handleSearch = event => {
    const searchQuery = event.target.value.toLowerCase();
    const searchResults = this.state.products.filter(el => {
      const searchTitleValue = el.title.toLowerCase();
      const searchDescValue = el.desc.toLowerCase();
      return (
        searchTitleValue.indexOf(searchQuery) !== -1 ||
        searchDescValue.indexOf(searchQuery) !== -1
      );
    });

    this.setState({ searchResults });
  };

  addToCart = id => {
    const currentItem = [...this.state.searchResults].filter(v => v.id === id);
    this.setState({
      ...this.state,
      cart: [...currentItem, ...this.state.cart]
    });
  };

  removeFromCart = productId => { 
    console.log('cart', this.state.cart)
    const updatedCartArray = [...this.state.cart].filter(v => v.id !== productId);
    console.log('updatedCartArray', updatedCartArray)
    this.setState({
      ...this.state,
      cart: updatedCartArray
    });
  };

  render() {
    return (
      <div className="app">
        <Container>
          <Header 
          cart={this.state.cart} 
          removeFromCart={this.removeFromCart}
          />
          <Search handleSearch={this.handleSearch} />
          <ProductsList
            products={this.state.searchResults}
            cart={this.state.cart}
            addToCart={this.addToCart} 
          />
        </Container>
      </div>
    );
  }
}

export default App;
