# Pizza Store

<img src="/public/assets/images/pizza-store.png">
<img src="/public/assets/images/pizza-store-search.png">
<img src="/public/assets/images/pizza-store-cart.png">


In the project directory, you can run:
### `npm install`
### `npm start`

-----------------------------
# Features:
  - live search by name and description
  - calculation of products in the cart window

# Tools: 
- react 16.6
- bootstrap 4 (reactstrap package)